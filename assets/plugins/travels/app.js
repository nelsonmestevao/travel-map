var mapOptions = {
    center: {lat: 38.957084, lng: -39.074225},
    zoom: 3,
    disableDefaultUI: true,
    backgroundColor: '#2b2b2b',
    styles: style
};

function addMarkers(map, points) {
    for (var i in points) {
        new google.maps.Marker({
            position: {lat: points[i].lat, lng: points[i].lng},
            title: points[i].title,
            icon: {
                path: google.maps.SymbolPath.CIRCLE,
                scale: 4,
                strokeWeight: 4,
                strokeColor: '#FF0066',
                fillColor: '#FF0066',
                fillOpacity: 1,
            },
            map: map
        });
    }
}

function addOverlayView(map) {
    var overlayView = new google.maps.OverlayView();
    overlayView.draw = function () {
        this.getPanes().markerLayer.id = 'markers';
    };
    overlayView.setMap(map);
}

function fitBounds(map, points) {
    var latLngBounds = new google.maps.LatLngBounds();
    for (var i = 0; i < points.length; i++) {
        latLngBounds.extend({lat: points[i].lat, lng: points[i].lng});
    }
    map.fitBounds(latLngBounds);
}

function addLegend(map, points) {
    var legend = document.getElementById('legend');
    legend.innerText = points.length + ' locations';

    map.controls[google.maps.ControlPosition.RIGHT_BOTTOM].push(legend);
}

function initMapCallback() {
    var map = new google.maps.Map(document.getElementById('map'), mapOptions);

    // Configure the map
    addMarkers(map, data.points);
    addOverlayView(map);
    addLegend(map, data.points);
    fitBounds(map, data.points);
}

// Load the google maps script dynamically so that we can configure the key
var googleMapsScript = document.createElement('script');
googleMapsScript.async = true;
googleMapsScript.defer = true;
googleMapsScript.src   = 'https://maps.googleapis.com/maps/api/js?callback=initMapCallback&key=' + data.apiKey;
document.getElementsByTagName('body')[0].appendChild(googleMapsScript);
