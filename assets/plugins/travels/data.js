var data = {
  apiKey: "AIzaSyDM3IO2owtaog_Ho40wY1ogpdEEfdgL4bs",
  points: [
    {
      title: "Brussels - Belgium",
      lat: 50.846723,
      lng: 4.352368
    },
    {
      title: "Bayonne - France",
      lat: 43.4929,
      lng: -1.4748
    },
    {
      title: "Grenoble - France",
      lat: 45.1885,
      lng: 5.7245
    },
    {
      title: "Marseille - France",
      lat: 43.2965,
      lng: 5.3698
    },
    {
      title: "Berlin - Germany",
      lat: 52.509718,
      lng: 13.3757
    },
    {
      title: "Bielefeld - Germany",
      lat: 52.0302,
      lng: 8.5325
    },
    {
      title: "Dortmund - Germany",
      lat: 51.5136,
      lng: 7.4653
    },
    {
      title: "Hannover - Germany",
      lat: 52.3759,
      lng: 9.732
    },
    {
      title: "Hamburg - Germany",
      lat: 53.5511,
      lng: 9.9937
    },
    {
      title: "Paderborn - Germany",
      lat: 51.7189,
      lng: 8.7575
    },
    {
      title: "Milan - Italy",
      lat: 45.4642,
      lng: 9.19
    },
    {
      title: "Rome - Italy",
      lat: 41.9028,
      lng: 12.4964
    },
    {
      title: "Aveiro - Portugal",
      lat: 40.6405,
      lng: -8.6538
    },
    {
      title: "Beja - Portugal",
      lat: 38.0153,
      lng: -7.8627
    },
    {
      title: "Braga - Portugal",
      lat: 41.5454,
      lng: -8.4265
    },
    {
      title: "Bragança - Portugal",
      lat: 41.8061,
      lng: -6.7567
    },
    {
      title: "Coimbra - Portugal",
      lat: 40.2033,
      lng: -8.4103
    },
    {
      title: "Lisbon - Portugal",
      lat: 38.7223,
      lng: -9.1393
    },
    {
      title: "Madeira - Portugal",
      lat: 32.7607,
      lng: -16.9595
    },
    {
      title: "Porto - Portugal",
      lat: 41.1579,
      lng: -8.6291
    },
    {
      title: "Setúbal - Portugal",
      lat: 38.5254,
      lng: -8.8941
    },
    {
      title: "Sintra - Portugal",
      lat: 38.8029,
      lng: -9.3816
    },
    {
      title: "Lobios - Spain",
      lat: 41.8936,
      lng: -8.0683
    },
    {
      title: "Vigo - Spain",
      lat: 42.2406,
      lng: -8.7207
    },
    {
      title: "Zürich - Switzerland",
      lat: 47.3769,
      lng: 8.5417
    },
  ]
};
